Some Mathematica notebooks.

- **cylindrical_cavity.nb:** graphs Bessel function solution for a cylindrical waveguide.
- **ring_resonator.nb:** graphs resonances for a ring resonator.